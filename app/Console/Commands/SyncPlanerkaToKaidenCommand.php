<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class SyncPlanerkaToKaidenCommand extends Command
{
    const KAITEN_POSITION_APPENDS = 1;
    const KAITEN_CREATE_CARD_ENDPOINT = '/api/latest/cards';
    const KAITEN_GET_CARDS_LIST_ENDPOINT = '/api/latest/cards';
    const PLANERKA_GET_EVENTS_ENDPOINT = '/rest/v1/event/';

    const CACHE_FILENAME = 'ids_cache.txt';

    protected $signature = 'sync:planerka-kaiden';
    protected $description = 'Command description';

    public function handle()
    {
        $credFile = [
           [
               'api_key_planerka' => '111111',
               'api_key_kaiten' => '111111',
               'kaiten_board_id' => '111111',
           ],
        ];

        //TODO 4 dnya
        $dates = [
//            now()->format('d.m.Y'),
            now()->addDay()->format('d.m.Y'),
//            now()->addDays(2)->format('d.m.Y'),
        ];

        foreach ($credFile as $creds) {
            if (!$creds['api_key_planerka'] || !$creds['api_key_kaiten'] || !$creds['kaiten_board_id']) {
                continue;
            }

            foreach ($dates as $date) {
                $data = $this->fetchPlanerkaDayData($creds['api_key_planerka'], $date);

                $preparedData = $this->prepareData($data, $date, $creds);

                $this->putToKaiten(
                    apiKey: $creds['api_key_kaiten'],
                    data: $preparedData,
                );
            }
        }

    }

    protected function fetchPlanerkaDayData(string $apiKey, string $date): array
    {
        $url = config('services.planerka.url') . self::PLANERKA_GET_EVENTS_ENDPOINT . "?date={$date}";

        try {
            $response = Http::withHeaders([
                'x-auth' => $apiKey,
            ])->get($url);
        } catch (\Throwable $e) {
            Log::error($e->getMessage());

            return [];
        }

        return $response->json();
    }

    protected function prepareData(array $data, string $date, array $creds): array
    {
        $exisingIds = $this->getCachedIds($creds);

        return collect($data)
            ->whereNotIn('id', $exisingIds)
            ->where('rejected', false)
            ->sortBy('startTime')
            ->map(function ($item) use ($creds) {
                $item['board_id'] = $creds['kaiten_board_id'];
                $item['position'] = self::KAITEN_POSITION_APPENDS;
                $item['due_date'] = $item['startTime'];
                $item['external_id'] = $item['id'];

                return $item;
            })
            ->toArray();
    }

    protected function getCachedIds($creds): array
    {
        $query = http_build_query([
            'board_id' => $creds['kaiten_board_id'],
            'with_due_date' => true,
            'archived' => false,
        ]);
        $url = config('services.kaiten.url') . self::KAITEN_GET_CARDS_LIST_ENDPOINT . "?$query";

        try {
            $response = Http::withToken($creds['api_key_kaiten'])->get($url);
        } catch (\Throwable $e) {
            Log::error($e->getMessage());

            return [];
        }

        $data = collect($response->json())->pluck('external_id');

        return $data->toArray();
    }

    protected function putToKaiten(string $apiKey, array $data)
    {
        $url = config('services.kaiten.url') . self::KAITEN_CREATE_CARD_ENDPOINT;

        foreach ($data as $card) {
            try {
                Http::withToken($apiKey)->post($url, $card);
            } catch (\Throwable $e) {
                Log::error($e->getMessage());
            }
        }
    }
}
